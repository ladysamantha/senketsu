# Welcome to senketsu ECM
*Cross-platform fork of https://github.com/SenseNet/sensenet*
*It's a cool project, and way more mature than this one, you should make sure to check it out first!*

The ~~first~~ second Open Source Enterprise Content Management platform for .NET! 

## Project goals
* Cross-platform
* .NET Core and web-based (primarily)
* Easy to install (à la Nuget/Chocolatey)
* FOSS all the way


## Contributing
All kinds of contributions are welcome! We are happy if you have an idea, bugfix or feature request to share with others. Please check out our [Contribution guide](CONTRIBUTING.md) for details.
